user_name = input("Имя пользователя: ")
user_age = int(input("Возраст пользователя: "))
user_city = input("Город, в котором живет пользователь: ")

print(f"\nВозраст по эльфийским стандартам: {user_age*100}")

dragon_age = 1400
print(f'Ваш возраст на пару с дракошей: {user_age + dragon_age}')

user_name_length = len(user_name)
print(f'Длина имени: {user_name_length} Символов!')